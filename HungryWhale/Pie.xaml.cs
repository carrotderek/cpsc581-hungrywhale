﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using libSMARTMultiTouch;
using libSMARTMultiTouch.Controls;
using System.Threading;

namespace HungryWhale
{
	public partial class Pie : DraggableBorder
	{	
        private int id;
        private Point pt;
        private Canvas canvas;
        private Player player;
        private Dictionary<int, Player> players;
        private Scoreboard scoreboard = Scoreboard.getInstance();
        private Whale whale = Whale.getInstance();
        private SpeechBubble speech = SpeechBubble.getInstance();

        //storyboards 
        Storyboard feedingDone;
        Storyboard Fail;
        Storyboard Win;

        //sound files
        MediaPlayer whaleEat = new MediaPlayer();
        MediaPlayer saulWin = new MediaPlayer();
        MediaPlayer fail = new MediaPlayer();

        #region initialize
        public Pie(Canvas canvas, Dictionary<int, Player> players, int id, Player player, Point pt)
		{
			this.InitializeComponent();
            this.canvas = canvas;
            this.player = player;
            this.id = id;
            this.pt = pt;
            this.players = players;

            this.IsRotateEnabled = false;
            this.MinScale = 1;
            this.MaxScale = 2;

            //we want a different pie from for each player
            this.pieRim.Fill = new LinearGradientBrush(getTop(), getBottom(), 90.0);
            this.TouchMove += new libSMARTMultiTouch.Input.TouchContactEventHandler(Pie_TouchMove);
            this.ScaleTransformUpdated += new EventHandler(Pie_ScaleTransformUpdated);
        }
        #endregion


        #region Handling touch events
        //when the pie is scaled, update the label
        void Pie_ScaleTransformUpdated(object sender, EventArgs e)
        {
			double w = this.ScaleTransform.ScaleX;

            if (w >= 2)
                this.pieSize.Content = 25;
            else if (w >= 1.66)
                this.pieSize.Content = 10;
            else if (w >= 1.33)
                this.pieSize.Content = 5;
            else
                this.pieSize.Content = 1;
        }

		//when the pie is moved, and what happens when the whale eats it
        void Pie_TouchMove(object sender, libSMARTMultiTouch.Input.TouchContactEventArgs e)
        {
            Pie temp = (Pie)sender;
            Point pt = new Point();
            Point ptTopLeft = new Point();
            Point ptBotRight = new Point();
            Point blowholeTopLeft = new Point();
            Point blowholeBotRight = new Point();
            Ellipse whaleMouth = whale.whaleMouth;

            //center coordinates of the pie being dragged
            pt.X = (double)this.GetValue(Canvas.LeftProperty) + this.TranslateTransform.X + (this.Width / 2);
            pt.Y = (double)this.GetValue(Canvas.TopProperty) + this.TranslateTransform.Y + (this.Height / 2);

            //its a bit hacked.. -- mouth coordinates
            ptTopLeft.X = (double)whaleMouth.GetValue(Canvas.LeftProperty) + 300;
            ptTopLeft.Y = (double)whaleMouth.GetValue(Canvas.TopProperty) + 300;
            ptBotRight.X = ptTopLeft.X + whaleMouth.Width;
            ptBotRight.Y = ptTopLeft.Y + whaleMouth.Height;

            #region feeding the whale
            if (pt.X > ptTopLeft.X && pt.X < ptBotRight.X && pt.Y > ptTopLeft.Y && pt.Y < ptBotRight.Y)
            {
                //whale eats the pies (remove it) and increment pie counter
                canvas.Children.Remove(this);

                Random r = new Random();
                int num = r.Next(1, 3);
                String uri = "";

                switch (num)
                {
                    case 1: uri = "whaleEat1.wav"; break;
                    case 2: uri = "whaleEat3.wav"; break;
                    case 3: uri = "whaleEat2.wav"; break;
                }
                
                //sound
                whaleEat.Open(new Uri(@uri, UriKind.Relative));
                whaleEat.Play();

                if (!this.whale.inAnimation)
                {
                    int count = Int16.Parse(this.pieSize.Content.ToString());
                    scoreboard.updatePieCount(this.id, this.player, count);

                    //animation
                    this.whale.inAnimation = true;
                    Storyboard eating = (Storyboard)whale.Resources["Eating"];
                    eating.Completed += new EventHandler(eating_Completed);
                    eating.Begin();
                }
            }//end if for checking mouth boundaries
            #endregion

            #region done feeding
            //blowhole coordinates
            blowholeTopLeft.X = (double)whale.blowhole.GetValue(Canvas.LeftProperty) + 310;
            blowholeTopLeft.Y = (double)whale.blowhole.GetValue(Canvas.TopProperty) + 200;
            blowholeBotRight.X = blowholeTopLeft.X + whale.blowhole.Width;
            blowholeBotRight.Y = blowholeTopLeft.Y + whale.blowhole.Height;

            if (pt.X > blowholeTopLeft.X && pt.X < blowholeBotRight.X && pt.Y > blowholeTopLeft.Y && pt.Y < blowholeBotRight.Y)
            {
                canvas.Children.Remove(this);
                processAnswer();
            }
            #endregion
        }

        //check the answer.
        void processAnswer()
        {
            int result = speech.FirstNum * speech.SecondNum;
            Trainer t = Trainer.getInstance();
            Player thePlayer = players[this.id];

            if (!this.whale.inAnimation)
            {

                this.whale.inAnimation = true;
                feedingDone = (Storyboard)whale.Resources["FeedingDone"];
                feedingDone.Completed += new EventHandler(feedingDone_Completed);
                feedingDone.Begin();

                if (thePlayer.Pies == result)
                {
                    scoreboard.updateScore(this.id, player);
                    speech.Intro.Content = "OoOoo He looks so happy! Good job!";

                    //sound
                    saulWin.Open(new Uri(@"SaulWin.wav", UriKind.Relative));
                    saulWin.Play();

                    //animation
                    this.whale.inAnimation = true;
                    Storyboard TrainerWin = (Storyboard)t.Resources["Win"];
                    Win = (Storyboard)whale.Resources["Win"];
                    Win.Completed += new EventHandler(Win_Completed);
                    TrainerWin.Begin();
                    Win.Begin();
                }

                else
                {
                    this.whale.inAnimation = true;
                    speech.Intro.Foreground = new SolidColorBrush(Colors.Red);
                    speech.Intro.Content = "Incorrect! What have you done to my whale?!";

                    //sound
                    fail.Open(new Uri(@"Fail.wav", UriKind.Relative));
                    fail.Play();

                    //animation
                    Fail = (Storyboard)whale.Resources["Fail"];
                    Fail.Completed += new EventHandler(Fail_Completed);
                    Fail.Begin();
                }
            }
        }

        void feedingDone_Completed(object sender, EventArgs e)
        {
            this.whale.inAnimation = false;
        }

        void Win_Completed(object sender, EventArgs e)
        {
            this.whale.inAnimation = false;
            reset();
        }

        void Fail_Completed(object sender, EventArgs e)
        {
            this.whale.inAnimation = false;
            reset();
        }

        //resets the state of the game back to original, except player score
        void reset()
        {
            //reset pie count to zero for each player
            for (int i = 0; i < players.Count; i++)
            {
                players[i].Pies = 0;
            }
            //resetting scoreboard
            scoreboard.orangeCount.Content = 0;
            scoreboard.greenCount.Content = 0;

            //reset speech bubble
            speech.Intro.Foreground = new SolidColorBrush(Colors.Black);
            speech.Intro.Content = "Well now, let's try this again!";
            speech.updateEquation();
        }

        #region Feeding Events

        void eating_Completed(object sender, EventArgs e)
        {
            this.whale.inAnimation = false;
        }
        #endregion
        #endregion

        #region color of pies
        //color for the top part of the pie rim
        public Color getTop()
        {
            if ((this.id == 0) || (this.id == 2))
                return Color.FromRgb(255, 131, 48);
            else if ((this.id == 1) || (this.id == 3))
                return Color.FromRgb(90, 169, 0);
            else
                return Color.FromRgb(169, 109, 0);
        }

        //get the bottom color of the gradient for the pie rim.
        public Color getBottom()
        {
            if ((this.id == 0) || (this.id == 2))
                return Color.FromRgb(150, 60, 0);
            else if ((this.id == 1) || (this.id == 3))
                return Color.FromRgb(60, 75, 0);
            else
                return Color.FromRgb(75, 48, 0);
        }
        #endregion
    }
}