﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Controls;

namespace HungryWhale
{
    public class Player
    {
        private int pieCount = 0;
        private int score = 0;

        public Player()
        {
        }
 
        public int Pies
        {
            get
            { return pieCount; }

            set
            { pieCount = value; }
        }

        public int Score
        {
            get
            { return score; }

            set
            { score = value; }
        }
    }
}
