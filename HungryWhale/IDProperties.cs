﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace HungryWhale
{
    // this class is essentially a player.
    // each ID has a given set of points and piecount
    class IDProperties
    {
        private Color myColor = Colors.Yellow;
        private int myID = 0;

        public IDProperties(int ID)
        {
            // Constructor  
            this.ID = ID;
        }

        public int ID
        {
            get
            { return myID; }
            set
            { myID = this.GetCorrectedID(value); }
        }

        #region Static Utilities
        private static int idCounter = 0;
        private static Dictionary<int, int> dictIDs = new Dictionary<int, int>();        // Given an mouse ID, find and record its equivalent zero-based ID (or return it if we have it)
        // As mentioned above, we do this because the emulator does not guarantee that
        // mice will be numbered from 0, 1, etc. 
        // Don't use a static lookup table (an alternative approach) because this may not work across machines 
        private int GetCorrectedID(int mouseID)
        {
            // If we have the mouseID, return the corrected one
            if (IDProperties.dictIDs.ContainsKey(mouseID))
            {
                return IDProperties.dictIDs[mouseID];
            }
            else
            {
                // We don't have it. Add it, where the corrected ID is the current
                // value of the mouseCounter (which was initially 0) then return it
                IDProperties.dictIDs.Add(mouseID, IDProperties.idCounter);
                IDProperties.idCounter++;
                return IDProperties.dictIDs[mouseID];
            }
        }
        #endregion
    }
}
