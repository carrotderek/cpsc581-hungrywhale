﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HungryWhale
{
	/// <summary>
	/// Interaction logic for SpeechBubble.xaml
	/// </summary>
	public partial class SpeechBubble : UserControl
	{
        private const int MULT_MAX = 12;
        private static SpeechBubble INSTANCE = new SpeechBubble();
        private int first = 0;
        private int second = 0;

		public SpeechBubble()
		{
			this.InitializeComponent();
		}

        public static SpeechBubble getInstance()
        {
            return INSTANCE;
        }

        public int FirstNum
        {
            get { return first; }
            set { first = value; }
        }

        public int SecondNum
        {
            get { return second; }
            set { second = value; }
        }

        public void updateEquation()
        {
            this.Equation.Content = generateEquation();
        }

        private String generateEquation()
        {
            Random rand = new Random();
            first = rand.Next(1, MULT_MAX);
            second = rand.Next(1, MULT_MAX);
            return first.ToString() + "x" + second.ToString();
        }

	}
}