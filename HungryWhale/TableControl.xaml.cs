﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using libSMARTMultiTouch.Controls;
using libSMARTMultiTouch.Input;

namespace HungryWhale
{
    /// <summary>
    /// Interaction logic for TableControl.xaml
    /// </summary>
    public partial class TableControl : TableApplicationControl
    {
        private const int PIE_SIZE = 50; //pie size 50x50
        private const int PLAYER_NUM = 2;

        private TouchCanvas canvas = new TouchCanvas();

        private Dictionary<int, IDProperties> dictIDProperties = new Dictionary<int, IDProperties>();
        public Dictionary<int, Player> players = new Dictionary<int, Player>();

        // This dictionary associates a ContactMarker with an ID. 
        // For demonstration, we will use the corrected ID
        private Dictionary<int, Pie> contactPies = new Dictionary<int, Pie>();
        private Player player1, player2;

        public TableControl()
        {
            canvas.Background = new SolidColorBrush(Colors.Transparent);
            InitializeComponent();
        }
            
        #region InitializeUI
        private void TableApplicationControl_Loaded(object sender, RoutedEventArgs e)
        {
            TableLayoutRoot.Children.Add(canvas);
            canvas.Background = new LinearGradientBrush(Color.FromRgb(79,134,187), Color.FromRgb(35,86,136), 90);

			//we are working with two players
            player1 = new Player();
            player2 = new Player();
            Trainer trainer = Trainer.getInstance();
            trainer.setCanvas(this.canvas);
            Whale whale = Whale.getInstance();
            SpeechBubble speech = SpeechBubble.getInstance();
            Scoreboard scoreboard = Scoreboard.getInstance();
            PieStash pieStash = PieStash.getInstance();

            speech.updateEquation();

            TouchInputManager.AddTouchContactDownHandler(pieStash, new TouchContactEventHandler(Stash_TouchDown));

            trainer.SetValue(Canvas.LeftProperty, 0.0);
            trainer.SetValue(Canvas.TopProperty, 0.0);
            whale.SetValue(Canvas.LeftProperty, 300.0);
            whale.SetValue(Canvas.TopProperty, 200.0);
			speech.SetValue(Canvas.LeftProperty, 250.0);
			speech.SetValue(Canvas.TopProperty, 10.0);
			scoreboard.SetValue(Canvas.RightProperty, 10.0);
			scoreboard.SetValue(Canvas.TopProperty, 10.0);
            pieStash.SetValue(Canvas.LeftProperty, 0.0);
            pieStash.SetValue(Canvas.BottomProperty, -30.0);
			
            canvas.Children.Add(whale);
            canvas.Children.Add(trainer);
			canvas.Children.Add(speech);
			canvas.Children.Add(scoreboard);
            canvas.Children.Add(pieStash);
        }
        #endregion

        #region TouchEvents
        private void Stash_TouchDown(object sender, TouchContactEventArgs e)
        {
            //ids associated with shit.
            IDProperties idProperties = this.GetID(e.TouchContact.ID);

            if (!players.ContainsKey(idProperties.ID))
            {
                switch (idProperties.ID)
                {
                    case 0: this.players.Add(idProperties.ID, player1); break;
                    case 1: this.players.Add(idProperties.ID, player2); break;
                    case 2: this.players.Add(idProperties.ID, player1); break;
                    case 3: this.players.Add(idProperties.ID, player2); break;
                }
            }

            Pie pie = new Pie(this.canvas, this.players, idProperties.ID, players[idProperties.ID], e.TouchContact.Position);
            pie.SetValue(Canvas.LeftProperty, e.TouchContact.Position.X);
            pie.SetValue(Canvas.TopProperty, e.TouchContact.Position.Y);
            this.canvas.Children.Add(pie);

            //its already known, so create a different pie for that user.. 
            if (!contactPies.ContainsKey(idProperties.ID))
            {
                this.contactPies.Add(idProperties.ID, pie);
            }
        }
        #endregion

        #region Helper
        // Given a mouse ID:
        // - create a dictionary entry storing IDProperties by mouse ID if we don't already have one
        // - return the IDProperties object that stores status information associated with that ID
        private IDProperties GetID(int mouseID)
        {
            // If we have the mouseID, return the corrected one
            if (dictIDProperties.ContainsKey(mouseID))
            {
                return dictIDProperties[mouseID];
            }
            else
            {
                // We don't have it. Add a new ID Property then return it
                IDProperties idProperties = new IDProperties(mouseID);
                dictIDProperties.Add(mouseID, idProperties);
                return idProperties;
            }
        }
        #endregion
    }
}
