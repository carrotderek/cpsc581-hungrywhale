﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HungryWhale
{
	/// <summary>
	/// Interaction logic for PieStash.xaml
	/// </summary>
	public partial class PieStash : UserControl
	{
        private static PieStash INSTANCE = new PieStash();

		public PieStash()
		{
			this.InitializeComponent();
		}

        public static PieStash getInstance()
        {
            return INSTANCE;
        }
	}
}