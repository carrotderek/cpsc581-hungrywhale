﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using libSMARTMultiTouch;
using libSMARTMultiTouch.Controls;
using System.Windows.Media.Animation;

namespace HungryWhale
{
	/// <summary>
	/// Interaction logic for Trainer.xaml
	/// </summary>
	public partial class Trainer : DraggableBorder
	{
        private static Trainer INSTANCE = new Trainer();
        private Canvas canvas;
		private Whale whale = Whale.getInstance();
        private MediaPlayer saulEaten = new MediaPlayer();

		public Trainer()
		{
			this.InitializeComponent();
            this.TouchMove += new libSMARTMultiTouch.Input.TouchContactEventHandler(Trainer_TouchMove);
            this.IsScaleEnabled = false;
        }

        public static Trainer getInstance()
        {
            return INSTANCE;
        }

        public void setCanvas(Canvas canvas)
        {
            this.canvas = canvas;
        }

        #region Touch Event Handling
        void Trainer_TouchMove(object sender, libSMARTMultiTouch.Input.TouchContactEventArgs e)
        {
			Point pt = new Point();
			Point ptTopLeft = new Point();
            Point ptBotRight = new Point();
            Ellipse whaleMouth = whale.whaleMouth;
            SpeechBubble speech = SpeechBubble.getInstance();
            canvas.Children.Remove(speech);
			
			//center coordinates of the pie being dragged
            pt.X = (double)this.GetValue(Canvas.LeftProperty) + this.TranslateTransform.X + (this.Width / 2);
            pt.Y = (double)this.GetValue(Canvas.TopProperty) + this.TranslateTransform.Y + (this.Height / 2);

            //its a bit hacked.. -- mouth coordinates
            ptTopLeft.X = (double)whaleMouth.GetValue(Canvas.LeftProperty) + 300;
            ptTopLeft.Y = (double)whaleMouth.GetValue(Canvas.TopProperty) + 200;
            ptBotRight.X = ptTopLeft.X + whaleMouth.Width;
            ptBotRight.Y = ptTopLeft.Y + whaleMouth.Height;

			#region feeding the whale
            if (pt.X > ptTopLeft.X && pt.X < ptBotRight.X && pt.Y > ptTopLeft.Y && pt.Y < ptBotRight.Y)
            {
                //whale eats the pies (remove it) and increment pie counter
                canvas.Children.Remove(this);
                Scoreboard scoreboard = Scoreboard.getInstance();
                PieStash stash = PieStash.getInstance();
                canvas.Children.Remove(speech);
                canvas.Children.Remove(stash);

                //sound
                saulEaten.Open(new Uri(@"saulEaten.wav", UriKind.Relative));
                saulEaten.Play();

                //animation
                if (!this.whale.inAnimation)
                {
                    this.whale.inAnimation = true;
                    Storyboard saulEat = (Storyboard)whale.Resources["SaulEat"];
                    saulEat.Completed += new EventHandler(eating_Completed);
                    saulEat.Begin();
                }
            }//end if for checking mouth boundaries
            #endregion
        }
		
		void eating_Completed(object sender, EventArgs e)
        {
            this.whale.inAnimation = false;
        }
        #endregion
    }
}