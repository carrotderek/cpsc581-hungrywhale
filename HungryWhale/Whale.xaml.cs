﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HungryWhale
{
	/// <summary>
	/// Interaction logic for Whale.xaml
	/// </summary>
	public partial class Whale : UserControl
	{
        private static Whale INSTANCE = new Whale();

		public Whale()
		{
			this.InitializeComponent();
		}

        public static Whale getInstance()
        {
            return INSTANCE;
        }
        public bool inAnimation = false;
	}
}