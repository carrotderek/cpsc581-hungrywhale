﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HungryWhale
{
	public partial class Scoreboard : UserControl
	{
        private static Scoreboard INSTANCE = new Scoreboard();

		public Scoreboard()
		{
			InitializeComponent();
		}

        public static Scoreboard getInstance()
        {
            return INSTANCE;
        }
		
		public void updatePieCount(int id, Player player, int pieCount)
		{
			player.Pies += pieCount;
			
			if ((id == 0) || (id == 2))
				orangeCount.Content = player.Pies;
			else if ((id == 1) || (id == 3))
				greenCount.Content = player.Pies;
		}

        public void updateScore(int id, Player player)
        {
            player.Score += 1;

            if ((id == 0) || (id == 2))
                orangeScore.Content = player.Score;
            else if ((id == 1) || (id == 3))
                greenScore.Content = player.Score;
        }
		
	}
}